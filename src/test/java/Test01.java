import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class Test01 {


        @BeforeSuite //Открывает страницу с радио


        public void openRadioPage(){

            RunFirefox.driver.get("http://www.di.fm/");
            RunFirefox.driver.manage().window().maximize();
            RunFirefox.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

        @Test       // Действия на открытой странице
        public void actionsOnRadioPage(){

            RadioPage.clickButtons();


        }

}
